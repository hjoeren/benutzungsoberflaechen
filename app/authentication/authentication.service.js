(function () {
    'use strict';

    angular
        .module('app')
        .factory('AuthenticationService', AuthenticationService);

    AuthenticationService.$inject = ['$http', '$cookieStore', '$rootScope', '$base64'];

    function AuthenticationService($http, $cookieStore, $rootScope, $base64) {
        var service = {
            login: login,
            setCredentials: setCredentials,
            clearCredentials: clearCredentials
        };
        return service;

        function login(username, password, successCallback, errorCallback) {
            $http.get('https://www.iwi.hs-karlsruhe.de/Intranetaccess/REST/credential/validate', {
                    headers: {
                        'Authorization': 'Basic ' + $base64.encode(username + ':' + password)
                    }
                })
                .then(function (response) {
                    console.log('credential/validate succeed');
                    successCallback(response);
                }, function (response) {
                    console.error('credential/validate failed with code ' + response.status);
                    errorCallback(response.status);
                });
        }

        function setCredentials(username, password, successCallback, errorCallback) {
            $http.get('https://www.iwi.hs-karlsruhe.de/Intranetaccess/REST/credential/encryptedpassword', {
                    headers: {
                        'Authorization': 'Basic ' + $base64.encode(username + ':' + password)
                    }
                })
                .then(function (response) {
                    console.log('credentials/encryptedpassword succeed');

                    var authData = $base64.encode(username + ':' + response.data);

                    $rootScope.globals = {
                      currentUser: {
                          username: username,
                          authData: authData
                      }
                    };

                    $http.defaults.headers.common.Authorization = 'Basic ' + authData;

                    successCallback(response);
                }, function (response) {
                    console.error('credentials/encryptedpassword failed with code ' + response.status);

                    clearCredentials();

                    errorCallback(response);
                });
        }

        function clearCredentials() {
            $rootScope.globals = {};
            $cookieStore.remove('globals');
            $http.defaults.headers.common.Authorization = 'Basic ';
        }
    }

})();