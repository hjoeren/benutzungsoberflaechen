(function () {
    'use strict';

    angular
        .module('app')
        .factory('JobofferService', JobofferService);

    JobofferService.$inject = ['$http'];

    function JobofferService($http) {
        var service = {
            getOfferTypes: getOfferTypes,
            getCountries: getCountries,
            getOffers: getOffers,
            getNotepad: getNotepad,
            postNotepad: postNotepad,
            deleteNotepad: deleteNotepad
        };
        return service;

        function getOfferTypes(successCallback, errorCallback) {
            $http.get('https://www.iwi.hs-karlsruhe.de/Intranetaccess/REST/joboffer/offertypes/all')
                .then(function (response) {
                    console.log('get joboffer/offertypes/all succeed');
                    successCallback(response.data);
                }, function (response) {
                    console.error('get joboffer/offertypes/all failed with status ' + response.status);
                    errorCallback(response.status);
                });
        }

        function getCountries(successCallback, errorCallback) {
            $http.get('https://www.iwi.hs-karlsruhe.de/Intranetaccess/REST/joboffer/countries/all')
                .then(function (response) {
                    console.log('get joboffer/countries/all succeed');
                    successCallback(response.data);
                }, function (response) {
                    console.error('get joboffer/countries/all failed with status ' + response.status);
                    errorCallback(response.status);
                });
        }

        function getOffers(offerType, tags, start, amount, successCallback, errorCallback) {
            $http.get('https://www.iwi.hs-karlsruhe.de/Intranetaccess/REST/joboffer/offers/' + offerType + '/' + tags + '/' + start + '/' + amount)
                .then(function (response) {
                    console.log('get joboffer/offers/... succeed');
                    successCallback(response.data);
                }, function (response) {
                    console.error('get joboffer/offers/... failed with status ' + response.status);
                    errorCallback(response.status);
                });
        }

        function getNotepad(offerType, tags, start, amount, successCallback, errorCallback) {
            $http.get('https://www.iwi.hs-karlsruhe.de/Intranetaccess/REST/joboffer/notepad/' + offerType + '/' + tags + '/' + start + '/' + amount)
                .then(function (response) {
                    console.log('get joboffer/notepad/... succeed');
                    successCallback(response.data);
                }, function (response) {
                    console.error('get joboffer/notepad/... failed with status ' + response.status);
                    errorCallback(response.status);
                });
        }

        function postNotepad(offerId, successCallback, errorCallback) {
            var wrappedOfferId = [];
            wrappedOfferId.push(offerId);
            $http.post('https://www.iwi.hs-karlsruhe.de/Intranetaccess/REST/joboffer/v2/notepad/offers', wrappedOfferId)
                .then(function (response) {
                    console.log('post joboffer/notepad/offers succeed');
                    successCallback();
                }, function (response) {
                    console.error('post joboffer/notepad/offers failed with status ' + response.status);
                    errorCallback(response.status);
                });
        }

        function deleteNotepad(offerId, successCallback, errorCallback) {
            $http.delete('https://www.iwi.hs-karlsruhe.de/Intranetaccess/REST/joboffer/v2/notepad/offer/' + offerId, {
                data: ''
            })
                .then(function (response) {
                    console.log('delete joboffer/notepad/offer succeed');
                    successCallback();
                }, function (response) {
                    console.error('delete joboffer/notepad/offer failed with status ' + response.status);
                    errorCallback(response.status);
                });
        }
    }

})();