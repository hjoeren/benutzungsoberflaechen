(function () {
    'use strict';

    angular
        .module('app')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$location', 'AuthenticationService'];

    function LoginController($location, AuthenticationService) {
        var vm = this;

        vm.login = login;

        AuthenticationService.clearCredentials();

        function login() {
            vm.dataLoading = true;
            AuthenticationService.login(vm.username, vm.password, function (response) {
                AuthenticationService.setCredentials(vm.username, vm.password, function (response) {
                    $location.path('/');
                }, function () {
                    vm.dataLoading = false;
                    vm.loginError = true;
                });
            }, function () {
                vm.dataLoading = false;
                vm.loginError = true;
            });
        }
    }

})();