(function () {
    'use strict';

    angular
        .module('app')
        .controller('NotepadController', NotepadController);

    NotepadController.$inject = ['JobofferService', '$rootScope', '$uibModal'];

    function NotepadController(JobofferService, $rootScope, $uibModal) {
        var vm = this;

        vm.openCompanyModal = openCompanyModal;
        vm.openOfferModal = openOfferModal;
        vm.removeFromNotepad = removeFromNotepad;
        vm.search = search;
        vm.loadOffers = loadOffers;

        vm.tags = '';
        var selectedTags = '';

        vm.offerType = {
            selection: null,
            options: []
        };
        var selectedOfferType = null;
        initializeOfferTypes();

        vm.country = {
            selection: null,
            options: []
        };
        var selectedCountry = null;
        initializeCountries();

        vm.totalHits = 0;
        vm.loadedOffers = 0;
        vm.offers = [];
        vm.companies = {};
        vm.countries = {};
        vm.industrialSectors = {};

        vm.page = 0;
        vm.toLoadOffers = $rootScope.isMobile ? 10 : -1;

        function initializeOfferTypes() {
            JobofferService.getOfferTypes(function (offerTypes) {
                vm.offerType.options = offerTypes;
            }, function (status) {
                initializeOfferTypes();
            });
        }

        function initializeCountries() {
            JobofferService.getCountries(function (countries) {
                vm.country.options = countries;
            }, function (status) {
                initializeCountries();
            });
        }

        function filter(offers) {
            var filteredOffers = [];
            if (selectedCountry != null) {
                angular.forEach(offers, function (offer) {
                    if (offer.countryId == selectedCountry.id) {
                        filteredOffers.push(offer);
                    }
                });
            } else {
                filteredOffers = offers;
            }
            return filteredOffers;
        }

        function search() {
            vm.offers = [];
            vm.page = 0;
            selectedTags = vm.tags;
            selectedOfferType = vm.offerType.selection;
            selectedCountry = vm.country.selection;
            loadOffers();
        }

        function loadOffers() {
            JobofferService.getNotepad(selectedOfferType.shortname, selectedTags, $rootScope.isMobile ? (vm.page - 1) * vm.toLoadOffers : 0, vm.toLoadOffers, function (offers) {
                vm.companies = offers.companies;
                vm.countries = offers.countries;
                vm.industrialSectors = offers.industrialSectors;
                vm.totalHits = offers.totalHits;
                vm.offers = filter(offers.offers);
            });
        }

        function openCompanyModal(companyId) {
            $uibModal.open({
                templateUrl: 'offers/company.modal.view.html',
                controller: 'CompanyModalController',
                controllerAs: 'vm',
                size: 'lg',
                resolve: {
                    company: function () {
                        return vm.companies[companyId];
                    },
                    country: function () {
                        return vm.countries[vm.companies[companyId].countryId];
                    },
                    industrialSectors: function () {
                        var industrialSectors = [];
                        angular.forEach(vm.companies[companyId].industrialSectorIds, function (industrialSectorId) {
                            industrialSectors.push(vm.industrialSectors[industrialSectorId]);
                        });
                        return industrialSectors;
                    }
                }
            });
        }

        function openOfferModal(offer) {
            $uibModal.open({
                templateUrl: 'offers/offer.modal.view.html',
                controller: 'OfferModalController',
                controllerAs: 'vm',
                size: 'lg',
                resolve: {
                    offer: function () {
                        return offer;
                    },
                    company: function () {
                        return vm.companies[offer.companyId];
                    },
                    country: function () {
                        return vm.countries[offer.countryId];
                    }
                }
            });
        }

        function removeFromNotepad(offerId) {
            JobofferService.deleteNotepad(offerId, function () {
                search();
            });
        }

    }

})();