(function () {
    'use strict';

    angular
        .module('app')
        .controller('TopnavController', TopnavController);

    TopnavController.$inject = ['$location', '$rootScope', 'AuthenticationService'];

    function TopnavController($location, $rootScope, AuthenticationService) {
        var vm = this;

        vm.loggedIn = {};

        (function() {
            $rootScope.$watch('globals.currentUser', function() {
                vm.loggedIn = $rootScope.globals.currentUser;
            });
        })();

        function changeView(view) {
            $location.path('#/' + view);
        }

        function logout() {
            AuthenticationService.clearCredentials();
        }
    }

})();