(function() {
    'use strict';

    angular
        .module('app')
        .controller('OfferModalController', OfferModalController);

    OfferModalController.$inject = ['$uibModalInstance', 'offer', 'country', 'company'];

    function OfferModalController($uibModalInstance, offer, country, company) {
        var vm = this;

        vm.offer = offer;
        vm.country = country;
        vm.company = company;

        vm.close = close;

        function close() {
            $uibModalInstance.dismiss('close');
        }
    }

})();