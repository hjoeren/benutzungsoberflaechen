(function() {
    'use strict';

    angular
        .module('app')
        .controller('CompanyModalController', CompanyModalController);

    CompanyModalController.$inject = ['$scope', '$uibModalInstance', 'company', 'country', 'industrialSectors'];

    function CompanyModalController($scope, $uibModalInstance, company, country, industrialSectors) {
        var vm = this;

        vm.company = company;
        vm.country = country;
        vm.industrialSectors = industrialSectors;

        vm.render = false;
        (function initController() {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({
                'address': vm.company.street + ' ' + vm.company.zipCode + ' ' + vm.company.city + ' ' + vm.country.name
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var location = results[0].geometry.location;
                    vm.center = [location.lat(), location.lng()];
                    vm.render = true;
                    $scope.$apply();
                }
            });
        })();

        vm.close = close;

        function close() {
            $uibModalInstance.dismiss('close');
        }

    }

})();