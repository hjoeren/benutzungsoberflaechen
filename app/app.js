(function () {
    'use strict';

    angular
        .module('app', ['ngRoute', 'ngCookies', 'ngSanitize', 'ui.bootstrap', 'base64', 'ngMap'])
        .config(config)
        .run(run);

    config.$inject = ['$httpProvider', '$routeProvider', '$locationProvider'];

    function config($httpProvider, $routeProvider, $locationProvider) {
        $httpProvider.defaults.useXDomain = true;
        //$httpProvider.defaults.withCredentials = true;

        $httpProvider.defaults.headers.post = {'Content-Type': 'application/json'};
        $httpProvider.defaults.headers.delete = {'Content-Type': 'application/json'};

        $routeProvider
            .when('/', {
                controller: 'OffersController',
                templateUrl: 'offers/offers.view.html',
                controllerAs: 'vm'
            })
            .when('/login', {
                controller: 'LoginController',
                templateUrl: 'login/login.view.html',
                controllerAs: 'vm'
            })
            .when('/offers', {
                controller: 'OffersController',
                templateUrl: 'offers/offers.view.html',
                controllerAs: 'vm'
            })
            .when('/notepad', {
                controller: 'NotepadController',
                templateUrl: 'notepad/notepad.view.html',
                controllerAs: 'vm'
            })
            .otherwise({
                redirectTo: '/login'
            });
    }

    run.$inject = ['$rootScope', '$location', '$cookieStore', '$http'];

    function run($rootScope, $location, $cookieStore, $http) {
        $rootScope.isMobile = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));

        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authData;
        }

        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            var restrictedPage = $.inArray($location.path(), ['/login']) === -1;
            var loggedIn = $rootScope.globals.currentUser;
            if (restrictedPage && !loggedIn) {
                $location.path('/login');
            }
        });
    }

})();